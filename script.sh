#!/bin/bash

# variables
config=
'''
    $CI_JOB_IMAGE="ENV DEBUG"\n
    $CI_JOB_VARIABLE==$SUPERSECRETVARIABLE
'''

# commands
sudo apt-get update -y && sudo apt-get install apache2 -y
sudo systemctl start apache2
sudo chown -R www-data:www-data /var/www/html/flag
echo -e $config | sudo tee /etc/systemd/system/gitlab-ci.service
sudo systemctl daemon-reload
sudo systemctl start gitlab-ci
sudo systemctl enable gitlab-ci
